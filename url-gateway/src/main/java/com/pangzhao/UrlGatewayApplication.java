package com.pangzhao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class UrlGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(UrlGatewayApplication.class);
    }

    /**
     * 转发到BILI
     *
     * @param builder
     * @return
     */
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("url-proxy-bili", r -> r.path("/bili")
                        .uri("https://www.bilibili.com/"))
                .build();
    }

}
