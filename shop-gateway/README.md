### 网关
路由(Route) 是 gateway 中最基本的组件之一，表示一个具体的路由信息载体。主要定义了下面的几个信息:
- id，路由标识符，区别于其他 Route。
- uri，路由指向的目的地 uri，即客户端请求最终被转发到的微服务。
- order，用于多个 Route 之间的排序，数值越小排序越靠前，匹配优先级越高。
- predicate，断言的作用是进行条件判断，只有断言都返回真，才会真正的执行路由。
- filter，过滤器用于修改请求和响应信息。