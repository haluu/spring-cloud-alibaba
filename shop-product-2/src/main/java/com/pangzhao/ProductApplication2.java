package com.pangzhao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient//将商品微服务注册到nacos
public class ProductApplication2 {
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication2.class);
    }
}
