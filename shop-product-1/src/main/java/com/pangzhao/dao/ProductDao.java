package com.pangzhao.dao;

import com.pangzhao.domain.Product;
import com.pangzhao.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDao extends JpaRepository<Product, Integer> {
}
