package com.pangzhao.service;

import com.pangzhao.domain.Order;

public interface OrderService {

    //创建订单
    void createOrder(Order order);
}
