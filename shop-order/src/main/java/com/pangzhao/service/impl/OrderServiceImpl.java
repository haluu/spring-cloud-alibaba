package com.pangzhao.service.impl;

import com.pangzhao.dao.OrderDao;
import com.pangzhao.domain.Order;
import com.pangzhao.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public void createOrder(Order order) {
        orderDao.save(order);
    }


}
