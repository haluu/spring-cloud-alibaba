package com.pangzhao.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.pangzhao.service.impl.OrderServiceImpl3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 微服务集成Sentinel
 */
@Slf4j
@RestController
@RequestMapping("/sentinel")
public class OrderController4 {

    @Resource
    private OrderServiceImpl3 orderServiceImpl3;
    //模拟异常情况
    int i;

    /**
     * 首先模拟一个异常
     *
     * @return
     */
    @RequestMapping("/order/message1")
    public String message1() {
        i++;
        if (i % 3 == 0) {
            throw new RuntimeException();
        }
        return "message1";
    }

    @RequestMapping("/order/message2")
    public String message2() {
        return "message2";
    }

    /**
     * 热点参数流控规则是一种更细粒度的流控规则, 它允许将规则具体到参数上
     *
     * @return
     */
    @RequestMapping("/order/message3")
    @SentinelResource("message3")
    public String message3(String name, Integer age) {
        return name + age;
    }

}
