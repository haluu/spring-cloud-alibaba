package com.pangzhao.dao;

import com.pangzhao.domain.Order;
import com.pangzhao.domain.TxLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TxLogDao extends JpaRepository<TxLog, String> {
}
