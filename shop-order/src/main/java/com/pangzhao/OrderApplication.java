package com.pangzhao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient//将商品微服务注册到nacos
@EnableFeignClients//开启fegin的客户端,基于Feign实现服务调用
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class);
    }

    /**
     * 基于Ribbon实现负载均衡
     * 第1步：在RestTemplate 的生成方法上添加@LoadBalanced注解
     *
     * @return
     */
    @Bean
    @LoadBalanced//基于Ribbon实现负载均衡
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
